<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id', 'brand_id', 'name', 'amount', 'code', 'photo_id', 'short_description', 'description',
        'meta_title', 'meta_description', 'meta_keywords', 'read', 'is_publish'
    ];
}
