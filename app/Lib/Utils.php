<?php
/**
 * Created by PhpStorm.
 * User: bpote
 * Date: 27/02/2019
 * Time: 22:40
 */

namespace App\Lib;
//use App\User;


class Utils
{
    public static function getConnectedUser()
    {
        $connectedUser =  auth()->user();
        return $connectedUser;
    }
}
