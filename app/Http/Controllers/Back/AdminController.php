<?php
namespace App\Http\Controllers\Back;

use App\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller

{
    public function index()
    {
        //Recuperer la liste des categories
        $categories = ProductCategory::all();
        return view('back.index', ['liste_categories' => $categories]);
    }

}
