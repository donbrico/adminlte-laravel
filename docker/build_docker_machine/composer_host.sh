wget https://getcomposer.org/installer -O composer-setup.php
php -r "if (hash_file('SHA384', 'composer-setup.php') === '55d6ead61b29c7bdee5cccfb50076874187bd9f21f65d8991d46ec5cc90518f447387fb9f76ebae1fbbacf329e583e30') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php --check
wget https://getcomposer.org/composer.phar -O composer.phar
rm -f composer-setup.php
sudo mv composer.phar /usr/local/bin/composer
chmod +x /usr/local/bin/composer
composer -V