#!/bin/bash

# temp. start mysql to do all the install stuff
/usr/bin/mysqld_safe > /dev/null 2>&1 &

# ensure mysql is running properly
sleep 20 

# install pimcore if needed
if [ ! -d /var/www/pimcore ]; then
  # clone
  cd /var/www
  rm -rf /var/www/*
  rm -rf /var/www/.*
  
  sudo -u www-data https_proxy=$https_proxy git clone https://github.com/donbrico/mikapote.git .
  
  # if pimcore-migration still exists, check it out. Once migration is done, this line will become inactive.
  #sudo -u www-data git show-ref --verify --quiet refs/remotes/origin/master && sudo -u www-data git checkout pimcore-migration

  sudo -u www-data composer install
  
#  sudo -u www-data mv /var/www/website/var/config/system.template.php /var/www/website/var/config/system.php
  sudo -u www-data cp /tmp/cache.php /var/www/website/var/config/cache.php
  
fi

  # create demo mysql user
  mysql -u root -e "CREATE USER 'user_pimcore'@'%' IDENTIFIED BY 'rMhv8uYhZ6GW5X6k';"
  mysql -u root -e "GRANT ALL PRIVILEGES ON *.* TO 'user_pimcore'@'%' WITH GRANT OPTION;"
  
  # setup database 
  mysql -u user_pimcore -prMhv8uYhZ6GW5X6k -e "CREATE DATABASE bdmikapotepimcore charset=utf8;"; 
  mysql -u user_pimcore -prMhv8uYhZ6GW5X6k bdmikapotepimcore < /var/www/pimcore/modules/install/mysql/install.sql
  mysql -u user_pimcore -prMhv8uYhZ6GW5X6k bdmikapotepimcore < /var/www/website/dump/data.sql
  
  # 'admin' password is 'demo' 
  mysql -u user_pimcore -prMhv8uYhZ6GW5X6k -D bdmikapotepimcore -e "UPDATE users SET id = '0' WHERE name = 'system'"
  
  sudo -u www-data php /var/www/pimcore/cli/console.php reset-password -u admin -p "&JkW!7Jq7RS"
  sudo -u www-data php /var/www/pimcore/cli/console.php deployment:classes-rebuild

# stop temp. mysql service
mysqladmin -uroot shutdown

#start livereload
cd /var/www/
php reload.phar server:run -- 0.0.0.0:35729 &

exec supervisord -n