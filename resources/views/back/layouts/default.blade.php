@inject('Utils','App\Lib\Utils')<!-- Main Header -->
<!DOCTYPE html>
<html>
<head>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CRM LARAVEL PROJECT | Brice POT&Eacute;</title>
    @include('back.includes.required_css')
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

@include('back.includes.global_header')
@include('back.includes.left_sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @include('back.includes.header')
          {{-- Debut contenu principal --}}
          @yield('main')
        {{-- Fin contenu principal --}}
    </div>
    <!-- /.content-wrapper -->

@include('back.includes.global_footer')
@include('back.includes.sidebar_control')

</div>
<!-- ./wrapper -->
@include('back.includes.required_js'))
</body>
</html>