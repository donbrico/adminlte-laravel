@extends('back.layouts.default')
@section('main')
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Catégories Produits</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>id</th>
                <th>parent_id</th>
                <th>lft</th>
                <th>rgt</th>
                <th>depth</th>
                <th>name</th>
                <th>crop_type</th>
                <th>created_at</th>
                <th>updated_at</th>
              </tr>
              </thead>
              <tbody>
              @foreach ($categories_list as $category)
              <tr>
                <td>{{$category->id}}</td>
                <td>{{$category->parent_id}}</td>
                <td>{{$category->lft}}</td>
                <td>{{$category->rgt}}</td>
                <td>{{$category->depth}}</td>
                <td>{{$category->name}}</td>
                <td>{{$category->crop_type}}</td>
                <td>{{$category->created_at}}</td>
                <td>{{$category->updated_at}}</td>
              </tr>
              @endforeach

              </tbody>
              <tfoot>
              <tr>
                <th>id</th>
                <th>parent_id</th>
                <th>lft</th>
                <th>rgt</th>
                <th>depth</th>
                <th>name</th>
                <th>crop_type</th>
                <th>created_at</th>
                <th>updated_at</th>
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
@endsection

@push('gestion-menu-ouvert')
  <script>
    $(function () {
      $('li.treeview').each(function() {
        if($( this ).hasClass('active menu-open')){
          $( this ).removeClass('active menu-open');
        }
      });
      $('li.treeview').addClass('active menu-open');
      $('#menu-category-product').addClass('active');
    });
  </script>
@endpush