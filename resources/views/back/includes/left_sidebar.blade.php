<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{Gravatar::image($Utils::getConnectedUser()->email)}}" class="img-circle" alt="User Image">
                <!-- <img src="/adminlte/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
            </div>
            <div class="pull-left info">
                <p>{{$Utils::getConnectedUser()->name}}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                <!--
                <br />
                <small>
                    <small>
                        <a href="#"><span><i class="fa fa-user-circle-o"></i>Mon compte</span></a> &nbsp;  &nbsp;
                        <a href="#"><i class="fa fa-sign-out"></i><span>Déconnexion</span></a>
                    </small>
                </small>
                -->
            </div>

        </div>

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">HEADER</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="/home"><i class="fa fa-link"></i> <span>Home</span></a></li>
            <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-link"></i> <span>Gestion des produits</span>
                    <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                </a>
                <ul class="treeview-menu" id="menu-category-product-tree">
                    <li id="menu-category-product"><a href="/categories">Catégories produits</a></li>
                    <li id="menu-product"><a href="/products">Produits</a></li>
                </ul>
            </li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>