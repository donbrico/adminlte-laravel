
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link href="{{ asset('adminlte/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
<!-- Font Awesome -->
<link href="{{ asset('adminlte/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<!-- Ionicons -->
<link href="{{ asset('adminlte/Ionicons/css/ionicons.min.css') }}" rel="stylesheet">

{{-- Uniquement pour la page contentant des data tables --}}
<link href="{{ asset('adminlte/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">

<!-- Theme style -->
<link href="{{ asset('adminlte/dist/css/AdminLTE.min.css') }}" rel="stylesheet">
<!-- AdminLTE Skins. We have chosen the skin-blue for this starter
      page. However, you can choose any other skin. Make sure you
      apply the skin class to the body tag so the changes take effect. -->
<link href="{{ asset('adminlte/dist/css/skins/skin-blue.min.css') }}" rel="stylesheet">

{{-- Uniquement pour la page d'accueil --}}
<link href="{{ asset('adminlte/morris.js/morris.css') }}" rel="stylesheet">
<link href="{{ asset('adminlte/jvectormap/jquery-jvectormap.css') }}" rel="stylesheet">
<link href="{{ asset('adminlte/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
<link href="{{ asset('adminlte/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
<link href="{{ asset('adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet">

@yield('css')

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">