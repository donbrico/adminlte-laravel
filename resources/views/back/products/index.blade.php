@extends('back.layouts.default')
@section('main')
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Produits</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>id</th>
                <th>Name</th>
                <th>created_at</th>
                <th>updated_at</th>
              </tr>
              </thead>
              <tbody>
              @foreach ($products_list as $product)
              <tr>
                <td>{{$product->id}}</td>
                <td>{{$product->name}}</td>
                <td>{{$product->created_at}}</td>
                <td>{{$product->updated_at}}</td>
              </tr>
              @endforeach

              </tbody>
              <tfoot>
              <tr>
                <th>id</th>
                <th>Name</th>
                <th>created_at</th>
                <th>updated_at</th>
              </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
@endsection

@push('gestion-menu-ouvert')
  <script>
    $(function () {
      $('li').each(function() {
        if($( this ).hasClass('active menu-open')){
          $( this ).removeClass('active menu-open');
        }
      });
      $('li.treeview').addClass('active menu-open');
      $('#menu-product').addClass('active');
    });
  </script>
@endpush