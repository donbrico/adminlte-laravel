<?php

use App\ProductCategory;
//use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(ProductCategory::class, function (Faker $faker) {
    return [
        'parent_id' => 0,
        'lft' => $faker->numberBetween(1,10),
        'rgt' => $faker->numberBetween(1,10),
        'depth' => $faker->numberBetween(1,10),
        'name' => $faker->name,
        'crop_type' => $faker->sentence()
    ];
});
