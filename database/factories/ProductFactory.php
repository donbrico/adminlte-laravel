<?php

use App\Product;
//use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Product::class, function (Faker $faker) {
    return [
        'category_id' => 1,
        'brand_id' => 2,
        'name' => $faker->name,
        'amount' => 100.00,
        'code' =>$faker->sentence(1),
        'photo_id' => 1,
        'short_description' => $faker->sentence(1),
        'description' => $faker->sentence(3),
        'meta_title' => $faker->sentence(),
        'meta_description' => $faker->sentence(2),
        'meta_keywords' => $faker->sentence(),
        'read' => 1,
        'is_publish' => 1
    ];
});
